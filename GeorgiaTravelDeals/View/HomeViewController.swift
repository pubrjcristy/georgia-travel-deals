//
//  HomeViewController.swift
//  GeorgiaTravelDeals
//
//  Created by dearwolves on 28Wednesday/08/20191.
//  Copyright © 2019 dearwolves. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class HomeViewController: UIViewController {

    @IBOutlet weak var toursVCView: UIView!
    @IBOutlet weak var countryView: UIImageView!
    @IBOutlet weak var navigationVCView: UIView!
    
    var homeViewModel = HomeViewModel()
    let disposeBag = DisposeBag()
    
    private lazy var tourViewController: TourTableViewVC = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "TourTableViewVC") as! TourTableViewVC
        self.add(asChildViewController: viewController, to: toursVCView)
        return viewController
    }()
    
    private lazy var navigationViewController: NavigationViewVC = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "NavigationViewVC") as! NavigationViewVC
        self.add(asChildViewController: viewController, to: navigationVCView)
        return viewController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        setupBindings()
        homeViewModel.generateDummyData()
    }
    
    private func setupBindings() {
        view.backgroundColor = UIColor(red: 244/255, green: 244/255, blue: 239/255, alpha: 1.0)

        homeViewModel
            .tours
            .observeOn(MainScheduler.instance)
            .bind(to: tourViewController.tours)
            .disposed(by: disposeBag)

        homeViewModel
            .countryImage
            .observeOn(MainScheduler.instance)
            .bind(to: countryView.rx.image)
            .disposed(by: disposeBag)
        
    }

    
}


