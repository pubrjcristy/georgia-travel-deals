//
//  NavigationViewVC.swift
//  GeorgiaTravelDeals
//
//  Created by dearwolves on 30Friday/08/20191.
//  Copyright © 2019 dearwolves. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class NavigationViewVC: UIViewController {
    
    @IBOutlet weak var logoButton: UIButton!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var cartButton: UIButton!
   
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupBindings()
    }
    
    private func setupBindings() {
        logoButton.rx.tap
            .bind {
                print("logo button tapped")
            }
            .disposed(by: disposeBag)
    }
}
