//
//  LoadingScreenController.swift
//  GeorgiaTravelDeals
//
//  Created by dearwolves on 30Friday/08/20191.
//  Copyright © 2019 dearwolves. All rights reserved.
//

import UIKit

class LoadingScreenController: UIViewController {
    
    @IBOutlet weak var loadingImage: UIImageView!
    var loadingImages: [UIImage] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let imageNamePrefix = "GeorgiaTravelDeals_android_launch_icons_flip_batch_blue"
        loadingImages = createLoadingImages(total: 12, imagePrefix: imageNamePrefix)
        animateLoadingImages(imageView: loadingImage, images: loadingImages)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
            self.performSegue(withIdentifier: "nextScreen", sender: nil)

        }
    }
        
        
    private func createLoadingImages(total: Int, imagePrefix: String) -> [UIImage] {
            
        var loadingImages: [UIImage] = []
            
        for imageCount in 1...total {
            let imageName = "\(imagePrefix)_\(imageCount)"
            print(imageName)
            let image = UIImage(named: imageName)!
            
            loadingImages.append(image)
        }
        return loadingImages
    }
    
    private func animateLoadingImages(imageView: UIImageView, images: [UIImage]) {
        imageView.animationImages = images
        imageView.animationDuration = 1.5
        imageView.animationRepeatCount = 3
        imageView.startAnimating()
    }
}
