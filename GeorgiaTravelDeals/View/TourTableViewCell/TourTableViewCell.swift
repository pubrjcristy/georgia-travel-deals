//
//  LocationTableViewCell.swift
//  GeorgiaTravelDeals
//
//  Created by dearwolves on 28Wednesday/08/20191.
//  Copyright © 2019 dearwolves. All rights reserved.
//

import UIKit

class TourTableViewCell: UITableViewCell {

    @IBOutlet weak var tourImage: UIImageView!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var tourNameLabel: UILabel!
    @IBOutlet weak var tourPriceLabel: UILabel!
    
    override var frame: CGRect {
        get {
            return super.frame
        }
        set (newFrame) {
            var frame =  newFrame
            frame.origin.y += 2
            frame.size.height -= 2 * 5
            super.frame = frame
        }
    }

    
    public var cellTour: Tour! {
        didSet {
            self.tourImage.clipsToBounds = true
            self.locationLabel.text = cellTour.tourLocation.uppercased()
            self.tourNameLabel.text = cellTour.tourName.capitalized
            self.tourPriceLabel.text = cellTour.tourPrice
            self.tourPriceLabel.adjustsFontSizeToFitWidth = true
            self.tourImage.image = cellTour.tourImage.withRenderingMode(.alwaysOriginal)
            self.tourImage.contentMode = .scaleToFill
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor(red: 254/255, green: 255/255, blue: 255/255, alpha: 1.0)
    }
    
    override func prepareForReuse() {
        tourImage.image = UIImage()
    }
}


