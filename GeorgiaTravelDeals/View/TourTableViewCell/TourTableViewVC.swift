//
//  TourTableViewVC.swift
//  GeorgiaTravelDeals
//
//  Created by dearwolves on 28Wednesday/08/20191.
//  Copyright © 2019 dearwolves. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class TourTableViewVC: UIViewController, UITableViewDelegate {

    @IBOutlet weak var tourTableView: UITableView!
    
    public var tours = PublishSubject<[Tour]>()
    
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupBindings()
    }
    
    private func setupBindings() {
        tourTableView.register(UINib(nibName: "TourTableViewCell", bundle: nil), forCellReuseIdentifier: String(describing: TourTableViewCell.self))
        tourTableView.backgroundColor = UIColor.clear
        tourTableView.separatorStyle = .none
        tourTableView.rx.setDelegate(self).disposed(by: disposeBag)
        tourTableView.contentInset = UIEdgeInsets(top: -20.0, left: 0.0, bottom: 0.0, right: 0.0);
        
        tours.bind(to: tourTableView.rx.items(cellIdentifier: "TourTableViewCell", cellType: TourTableViewCell.self)) { (row, tour, cell) in
            cell.cellTour = tour
        }.disposed(by: disposeBag)
        
        tourTableView.rx.willDisplayCell
            .subscribe(onNext: ({ (cell, indexPath) in
                cell.alpha = 1
                /*cell.layer.cornerRadius = 8
                cell.backgroundColor = UIColor.clear
                let transform = CATransform3DTranslate(CATransform3DIdentity, -250, 0, 0)
                cell.layer.transform = transform
                UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
                    cell.alpha = 1
                    cell.layer.transform = CATransform3DIdentity
                }, completion: nil)*/

            })).disposed(by: disposeBag)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("table index tapped: \(indexPath)")
    }
}
