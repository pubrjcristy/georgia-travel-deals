//
//  HomeViewModel.swift
//  GeorgiaTravelDeals
//
//  Created by dearwolves on 28Wednesday/08/20191.
//  Copyright © 2019 dearwolves. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class HomeViewModel {
    public let  tours: PublishSubject<[Tour]> = PublishSubject()
    public let countryTours: PublishSubject<Country> = PublishSubject()
    public let countryImage: PublishSubject<UIImage> = PublishSubject()
    
    private let disposable = DisposeBag()
    
    public func generateDummyData() {
        let tour1 = Tour(tourLocation: "USA, WASHINGTON DC", tourName: "2 hr Washington DC Unveiled tour", tourPrice: " From AUD $87.55", tourImage: #imageLiteral(resourceName: "washington_dc"))
        let tour2 = Tour(tourLocation: "Philippines, Cebu", tourName: "Cebu Travel Tour", tourPrice: " From PH ₱2,000.00", tourImage: #imageLiteral(resourceName: "cebu_tour"))
        let tour3 = Tour(tourLocation: "Philippines, Bohol", tourName: "Bohol Travel Tour", tourPrice: " From PH ₱3,500.00", tourImage: #imageLiteral(resourceName: "bohol_tour"))

        let tourList = [tour1, tour2, tour3]
        
        let country = Country(countryName: "USA", countryImage: #imageLiteral(resourceName: "usa_country"), tours: tourList)
        
        countryTours.onNext(country)
        tours.onNext(country.tours)
        countryImage.onNext(country.countryImage)
    }
}

