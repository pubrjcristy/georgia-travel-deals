//
//  Extensions.swift
//  GeorgiaTravelDeals
//
//  Created by dearwolves on 28Wednesday/08/20191.
//  Copyright © 2019 dearwolves. All rights reserved.
//

import UIKit

extension UIViewController {
    public func add(asChildViewController viewController: UIViewController,to parentView:UIView) {
        // Add Child View Controller
        addChild(viewController)
        
        // Add Child View as Subview
        parentView.addSubview(viewController.view)
        
        // Configure Child View
        viewController.view.frame = parentView.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        // Notify Child View Controller
        viewController.didMove(toParent: self)
    }
}
