//
//  Country.swift
//  GeorgiaTravelDeals
//
//  Created by dearwolves on 29Thursday/08/20191.
//  Copyright © 2019 dearwolves. All rights reserved.
//

import UIKit

struct Country {
    let countryName: String
    let countryImage: UIImage
    let tours: [Tour]
}
