//
//  Tour.swift
//  GeorgiaTravelDeals
//
//  Created by dearwolves on 28Wednesday/08/20191.
//  Copyright © 2019 dearwolves. All rights reserved.
//

import Foundation
import UIKit

struct Tour {
    let tourLocation, tourName, tourPrice: String
    let tourImage: UIImage
}
